<?php

namespace Drupal\expression\Context;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Plugin\Context\ContextInterface;

class ContextBag implements ContextBagInterface {

  /**
   * @var \Drupal\Core\Plugin\Context\ContextInterface[]
   */
  protected $contexts = [];

  /**
   * @var bool[]
   */
  protected $updatables = [];

  /**
   * @param string $name
   * @param \Drupal\Core\Plugin\Context\ContextInterface $context
   *
   * @return \Drupal\expression\Context\ContextBagInterface
   */
  public function addContext(string $name, ContextInterface $context): ContextBagInterface {
    if (isset($this->contexts[$name])) {
      throw new \RuntimeException("Can not add context twice: $name");
    }
    $this->contexts[$name] = $context;
    $this->updatables[$name] = FALSE;
    return $this;
  }

  /**
   * @param string $name
   * @param \Drupal\Core\Plugin\Context\ContextInterface $context
   *
   * @return \Drupal\expression\Context\ContextBagInterface
   */
  public function addUpdatableContext(string $name, ContextInterface $context): ContextBagInterface {
    $this->addContext($name, $context);
    $this->updatables[$name] = TRUE;
    return $this;
  }

  /**
   * @return \Drupal\Core\Plugin\Context\ContextInterface[]
   */
  public function getContexts(): array {
    return $this->contexts;
  }

  /**
   * @inheritDoc
   */
  public function hasContext(string $name): bool {
    return isset($this->contexts[$name]);
  }

  /**
   * @param string $name
   * @return \Drupal\Core\Plugin\Context\ContextInterface
   */
  public function getContext(string $name): ContextInterface {
    if (!isset($this->contexts[$name])) {
      throw new \RuntimeException("Can not get context: $name");
    }
    return $this->contexts[$name];
  }

  /**
   * @inheritDoc
   */
  public function isUpdatable(string $name): bool {
    return $this->updatables[$name] ?? FALSE;
  }

  /**
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $cacheableDependency
   */
  public function addCacheableDependencyToUpdatables(CacheableDependencyInterface $cacheableDependency): void {
    foreach ($this->contexts as $name => $context) {
      if ($this->updatables[$name]) {
        $context->addCacheableDependency($cacheableDependency);
      }
    }
  }

  /**
   * @param string $name
   * @param \Drupal\Core\Plugin\Context\ContextInterface $context
   * @param bool $allowNew
   *
   * @return $this
   */
  public function createUpdatedBag(string $name, ContextInterface $context, bool $allowNew = FALSE): ContextBag {
    if (!($this->updatables[$name] ?? $allowNew)) {
      throw new \RuntimeException("Can not update context: $name");
    }
    $updatedBag = new static();
    $updatedBag->updatables = $this->updatables + [$name => TRUE];
    $updatedBag->contexts = $this->contexts;
    $updatedBag->contexts[$name] = $context;
    return $updatedBag;
  }

}
