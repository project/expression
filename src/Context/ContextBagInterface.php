<?php

namespace Drupal\expression\Context;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Plugin\Context\ContextInterface;

interface ContextBagInterface {

  /**
   * @param string $name
   * @param \Drupal\Core\Plugin\Context\ContextInterface $context
   *
   * @return \Drupal\expression\Context\ContextBagInterface
   */
  public function addContext(string $name, ContextInterface $context): ContextBagInterface;

  /**
   * @param string $name
   * @param \Drupal\Core\Plugin\Context\ContextInterface $context
   *
   * @return \Drupal\expression\Context\ContextBagInterface
   */
  public function addUpdatableContext(string $name, ContextInterface $context): ContextBagInterface;

    /**
   * @return \Drupal\Core\Plugin\Context\ContextInterface[]
   */
  public function getContexts(): array;

  /**
   * @param string $name
   *
   * @return bool
   */
  public function hasContext(string $name): bool;

  /**
   * @param string $name
   *
   * @return \Drupal\Core\Plugin\Context\ContextInterface
   */
  public function getContext(string $name): ContextInterface;

  /**
   * @param string $name
   *
   * @return bool
   */
  public function isUpdatable(string $name): bool;

  /**
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $cacheableDependency
   */
  public function addCacheableDependencyToUpdatables(CacheableDependencyInterface $cacheableDependency): void;

    /**
   * @param string $name
   * @param \Drupal\Core\Plugin\Context\ContextInterface $context
   * @param bool $allowNew
   *
   * @return $this
   */
  public function createUpdatedBag(string $name, ContextInterface $context, bool $allowNew = FALSE): ContextBag;

}