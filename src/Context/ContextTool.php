<?php

namespace Drupal\expression\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\ContextInterface;

final class ContextTool {

  /**
   * @var \Drupal\expression\Sandboxing\SandboxerInterface
   */
  protected static $sandboxer;

  public static function ensureType(ContextInterface $context): ContextInterface {
    // Nothing to do, compiler checks type for us.
    // @todo Remove this once possible: At least PHP return type variance of
    //   CacheabilityMonad is needed.
    return $context;
  }

  public static function create($contextValue): ContextInterface {
    // If methods return values already wrapped in ContextInterface, use that.
    if ($contextValue instanceof ContextInterface) {
      return $contextValue;
    }
    else {
      return new Context(ContextDefinition::create()->setRequired(FALSE), $contextValue);
    }
  }

  /**
   * @param \Drupal\Core\Plugin\Context\ContextInterface[] $contexts
   */
  public static function getSandboxedValues($contexts) {
    $sandboxedValues = array_map(function (ContextInterface $context) {
      return static::getSandboxer()->wrap($context->getContextValue());
    }, $contexts);
    return $sandboxedValues;
  }

  /**
   * Merge cacheabilities, used by the compiler.
   *
   * @param \Drupal\Core\Cache\CacheableMetadata ...$cacheabilities
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   */
  public static function mergeCacheability(CacheableMetadata ...$cacheabilities): CacheableMetadata {
    $result = new CacheableMetadata();
    array_walk($cacheabilities, function ($item) use (&$result) {
      $result = $result->merge($item);
    });
    return $result;
  }

  private static function getSandboxer() {
    return static::$sandboxer ?? (static::$sandboxer = \Drupal::service('expression.sandboxer'));
  }

}
