<?php

namespace Drupal\expression\MonadicExpressionLanguage\CacheabilityMonad;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Plugin\Context\ContextInterface;
use Drupal\expression\Context\ContextTool;
use Drupal\expression\MonadicExpressionLanguage\MonadInterface;
use Drupal\expression\MonadicExpressionLanguage\PrecompiledNode;
use Drupal\expression\Sandboxing\SandboxerInterface;
use Symfony\Component\ExpressionLanguage\Compiler;
use Symfony\Component\ExpressionLanguage\Node\ConditionalNode;
use Symfony\Component\ExpressionLanguage\Node\ConstantNode;
use Symfony\Component\ExpressionLanguage\Node\FunctionNode;
use Symfony\Component\ExpressionLanguage\Node\GetAttrNode;
use Symfony\Component\ExpressionLanguage\Node\NameNode;
use Symfony\Component\ExpressionLanguage\Node\Node;
use Symfony\Component\ExpressionLanguage\SyntaxError;

class CacheabilityMonad implements MonadInterface {

  /**
   * @var \Drupal\expression\Sandboxing\SandboxerInterface
   */
  protected $sandboxer;

  public function __construct(SandboxerInterface $sandboxer) {
    $this->sandboxer = $sandboxer;
  }

  public function validateAst(Node $node, array $functions, array $names) {
    if ($node instanceof ConstantNode) {
      return;
    }
    elseif ($node instanceof NameNode) {
      return;
    }
    elseif ($node instanceof GetAttrNode) {
      // Method and property calls starting with an underscore are illegal here.
      if (
        in_array($node->attributes['type'], [
          GetAttrNode::PROPERTY_CALL,
          GetAttrNode::METHOD_CALL
        ])
        && ($name = $node->nodes['attribute']->attributes['value'])
        && substr($name, 0, 1) === '_'
      ) {
        throw new SyntaxError("Calling property or method starting with underscore is not allowed: $name");
      }
    }
    else {
      foreach ($node->nodes as $childNode) {
        $this->validateAst($childNode, $functions, $names);
      }
    }
  }

  /**
   * @param \Symfony\Component\ExpressionLanguage\Node\Node $node
   * @param array $functions
   * @param \Drupal\Core\Plugin\Context\ContextInterface[] $contexts
   *
   * @return \Drupal\Core\Plugin\Context\ContextInterface
   */
  public function evaluate(Node $node, array $functions, array $contexts) {
    $sandboxedValues = ContextTool::getSandboxedValues($contexts);
    // Do algebraic induction, with NameNode & ConstantNode as terminals.
    if ($node instanceof NameNode) {
      // For names (=variables), get value and ensure type.
      // Note that this is the only place where we call $node->evaluate with
      // un-sandboxed values, and it's what we want here.
      $result = $node->evaluate($functions, $contexts);
      return ContextTool::ensureType($result);
    }
    elseif ($node instanceof ConstantNode) {
      // For constants, wrap with empty cacheability data.
      // No variables are used here.
      $result = $node->evaluate($functions, []);
      return ContextTool::create($result);
    }
    elseif ($node instanceof ConditionalNode) {
      // One clue of a conditional is to not evaluate expressions that would
      // throw, so the default path to evaluate all sub-expressions is no good
      // here. So re-implement ConditionalNode::evaluate here.
      $conditionResult = $this->evaluate($node->nodes['expr1'], $functions, $contexts);
      $resultNode = $conditionResult->getContextValue() ? $node->nodes['expr2'] : $node->nodes['expr3'];
      $result = $this->evaluate($resultNode, $functions, $contexts);
      $result->addCacheableDependency($conditionResult);
      return $result;
    }
    else {
      // For all other (aggregate) nodes, ...
      // Prepare a cloned node to evaluate the value part.
      $valueNode = self::deepClone($node);
      $cacheability = new CacheableMetadata();
      // Recursively evaluate all child nodes.
      foreach ($this->childNodes($valueNode) as $childKey => &$childNode) {
        $childResult = $this->evaluate($childNode, $functions, $contexts);
        $resultValue = $childResult->getContextValue();
        // Replace child node refs with item constants.
        // Sticking sandboxed entities into a constant node won't dump in a
        // reasonable way, but it is fine to evaluate.
        $childNode = new ConstantNode($resultValue);
        $cacheability->addCacheableDependency($childResult);
      }
      // Evaluate the resulting item node.
      $resultValue = $valueNode->evaluate($functions, $sandboxedValues);

      // Put value and cacheability part together again.
      // ContextTool will pick up any value cacheability that was not part of
      // the child nodes.
      return ContextTool::create($resultValue)
        ->addCacheableDependency($cacheability);
    }
  }

  public function compile(Compiler $compiler, Node $node) {
    $compiler->raw($this->subcompile($compiler, $node));
  }

  public function subcompile(Compiler $compiler, Node $node): string {
    // @todo This is completely untested!
    $compiled = [];
    // Do algebraic induction, with NameNode & ConstantNode as terminals.
    if ($node instanceof NameNode) {
      $compiled[] = ContextTool::class . '::ensureType(';
      $compiled[] = $compiler->subcompile($node);
      $compiler->raw(')');
    }
    elseif ($node instanceof ConstantNode) {
      // For constants, wrap with empty cacheability data.
      $compiled[] = ContextTool::class . '::create(';
      $compiled[] = $compiler->subcompile($node);
      $compiled[] = ', NULL)';
    }
    else {
      // For all other (aggregate) nodes, ...

      // Compile children.
      $compiledChildren = [];
      foreach ($this->childNodes($node) as $key => $childNode) {
        $compiledChildren[$key] = $this->subcompile($compiler, $childNode);
      }

      // Compile the item part.
      $itemNode = clone $node;
      foreach ($this->childNodes($itemNode) as $key => &$childNode) {
        $childNode = new PrecompiledNode("({$compiledChildren[$key]})->_item()");
      }
      $compiledItem = $compiler->subcompile($itemNode);

      // Compile the cacheability part.
      $compiledCacheabilities = [];
      foreach ($compiledChildren as $key => $compiledChild) {
        $compiledCacheabilities[$key] = "($compiledChild)->_cacheability()";
      }

      if ($node instanceof ConditionalNode) {
        $compiledCacheability = '(('
          . "({$compiledChildren['expr1']})->_item()"
          . ') ? ('
          . ContextTool::class . '::mergeCacheability('
          . $compiledCacheabilities['expr1'] . ', ' . $compiledCacheabilities['expr2'] . ')'
          . ') : ('
          . ContextTool::class . '::mergeCacheability('
          . $compiledCacheabilities['expr1'] . ', ' . $compiledCacheabilities['expr3'] . ')'
          . '))'
          ;
      }
      else {
        $compiledCacheability = ContextTool::class . '::mergeCacheability('
          . implode(', ', $compiledCacheabilities) . ')';
      }

      // Wrap everything together.
      $compiled[] = ContextTool::class . '::create(';
      $compiled[] = $compiledItem;
      $compiler->raw(", \n");
      $compiled[] = $compiledCacheability;
      $compiler->raw(')');
    }
    return implode('', $compiled);
  }

  protected static function deepClone(Node $node): Node {
    $clone = clone $node;
    foreach ($clone->nodes as &$node) {
      $node = self::deepClone($node);
    }
    return $clone;
  }

  /**
   * Get child nodes.
   *
   * We wouldn't need this if any node only evaluated one level deep. But
   * function and method calls look two level deep into argument nodes,
   * so we have to make this voodoo.
   *
   * @param \Symfony\Component\ExpressionLanguage\Node\Node $node
   *
   * @return \Traversable
   */
  private function &childNodes(Node $node): \Traversable {
    if ($node instanceof FunctionNode) {
      foreach ($node->nodes['arguments']->nodes as $key => &$item) {
        yield $key => $item;
      }
    }
    elseif ($node instanceof GetAttrNode) {
      if ($node->attributes['type'] === GetAttrNode::PROPERTY_CALL) {
        yield 'node' => $node->nodes['node'];
      }
      elseif ($node->attributes['type'] === GetAttrNode::METHOD_CALL) {
        yield 'node' => $node->nodes['node'];
        foreach ($node->nodes['arguments']->nodes as $key => &$item) {
          yield $key => $item;
        }
      }
      elseif ($node->attributes['type'] === GetAttrNode::ARRAY_CALL) {
        yield 'node' => $node->nodes['node'];
        yield 'attribute' => $node->nodes['attribute'];
      }
    }
    else {
      // ArrayNode, BinaryNode, ConditionalNode, UnaryNode
      // Empty for: ConstantNode, NameNode
      // ArgumentsNode is used in FunctionsNode, GetAttrNode and handled there.
      foreach ($node->nodes as $key => &$item) {
        // Preserve key as that is used for ConditionalNode cacheability.
        yield $key => $item;
      }
    }
  }

}
