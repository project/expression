<?php

namespace Drupal\expression\MonadicExpressionLanguage\CacheabilityMonad;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedInterface;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedWrapperInterface;

final class ItemPlusCacheability {

  /**
   * @var \Drupal\expression\Sandboxing\Sandboxed\SandboxedWrapperInterface
   */
  private $item;

  /**
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  private $cacheability;

  /**
   * @var \Drupal\expression\Sandboxing\SandboxerInterface
   */
  private static $sandboxer;

  /**
   * ValuePlusCacheability constructor.
   *
   * @param mixed $item
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheability
   */
  private function __construct($item, CacheableMetadata $cacheability) {
    if (is_object($item) && !$item instanceof SandboxedInterface) {
      throw new \LogicException('Item must be non-object or sandboxed. Got: ' . get_class($item));
    }
    $this->item = $item;
    $this->cacheability = $cacheability;
  }

  /**
   * Static factory, used by the compiler.
   *
   * @param mixed $item
   * @param \Drupal\Core\Cache\CacheableDependencyInterface|\Drupal\Core\Cache\CacheableDependencyInterface[]|null $additionalCacheability
   *
   * @return static
   */
  public static function create($item, ?CacheableDependencyInterface $firstAdditionalCacheability, ?CacheableDependencyInterface ...$additionalCacheabilities) {
    if (!static::$sandboxer) {
      static::$sandboxer = \Drupal::service('expression.sandboxer');
    }
    $wrapped = static::$sandboxer->wrap($item);
    $cacheability = new CacheableMetadata();
    if (
      $wrapped instanceof SandboxedWrapperInterface
      && ($itemCacheability = $wrapped->_getCacheability())
    ) {
      $cacheability = $cacheability->merge(CacheableMetadata::createFromObject($itemCacheability));
    }
    foreach (array_merge([$firstAdditionalCacheability], $additionalCacheabilities) as $additionalCacheability) {
      if ($additionalCacheability) {
        $cacheability = $cacheability->merge(CacheableMetadata::createFromObject($additionalCacheability));
      }
    }

    return new static($wrapped, $cacheability);
  }

  /**
   * Merge cacheabilities, used by the compiler.
   *
   * @param \Drupal\Core\Cache\CacheableMetadata ...$cacheabilities
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   */
  public static function mergeCacheability(CacheableMetadata ...$cacheabilities): CacheableMetadata {
    $result = new CacheableMetadata();
    array_walk($cacheabilities, function ($item) use (&$result) {
      $result = $result->merge($item);
    });
    return $result;
  }

  public static function ensureType($item) {
    if (!$item instanceof ItemPlusCacheability) {
      throw new \UnexpectedValueException("Expected ItemPlusCacheability.");
    }
    return $item;
  }

  /**
   * @inheritDoc
   */
  public function _item() {
    return $this->item;
  }

  /**
   * @inheritDoc
   */
  public function _cacheability(): CacheableMetadata {
    return $this->cacheability;
  }

}
