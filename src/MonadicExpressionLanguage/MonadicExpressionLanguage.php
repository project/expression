<?php

namespace Drupal\expression\MonadicExpressionLanguage;

use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\ExpressionLanguage\Compiler;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

class MonadicExpressionLanguage extends ExpressionLanguage {

  /**
   * @var \Drupal\expression\MonadicExpressionLanguage\MonadInterface
   */
  private $monad;

  /**
   * @var \Symfony\Component\ExpressionLanguage\Compiler
   */
  private $compiler;

  /**
   * @inheritDoc
   */
  public function __construct(CacheItemPoolInterface $cache = NULL, array $providers = [], MonadInterface $monad = NULL) {
    parent::__construct($cache, $providers);
    $this->monad = $monad ?? new IdentityMonad();
  }

  public function __serialize() {
    return [
      // Omit cache, as ArrayAdapter is not serializable.
      'functions' => $this->functions,
      'monad' => $this->monad,
    ];
  }

  public function __unserialize(array $data) {
    $this->functions = $data['functions'];
    $this->monad = $data['monad'];
  }

  /**
   * @inheritDoc
   */
  public function evaluate($expression, $values = []) {
    $node = $this->parse($expression, array_keys($values))->getNodes();
    return $this->monad->evaluate($node, $this->functions, $values);
  }

  /**
   * @inheritDoc
   */
  public function compile($expression, $names = []) {
    $node = $this->parse($expression, $names)->getNodes();
    // Inline private parent function.
    $compiler = $this->compiler ?? ($this->compiler = new Compiler($this->functions));
    return $this->monad->compile($compiler, $node)->getSource();
  }

  /**
   * @inheritDoc
   */
  public function parse($expression, $names) {
    $result = parent::parse($expression, $names);
    $this->monad->validateAst($result->getNodes(), $this->functions, $names);
    return $result;
  }

  protected function registerFunctions() {
    // Override this method to not register the constant() function.
  }

}
