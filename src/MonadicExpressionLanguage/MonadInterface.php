<?php

namespace Drupal\expression\MonadicExpressionLanguage;

use Symfony\Component\ExpressionLanguage\Compiler;
use Symfony\Component\ExpressionLanguage\Node\Node;

interface MonadInterface {

  public function validateAst(Node $node, array $functions, array $names);

  public function evaluate(Node $node, array $functions, array $values);

  public function compile(Compiler $compiler, Node $node);

}
