<?php

namespace Drupal\expression\MonadicExpressionLanguage;

use Symfony\Component\ExpressionLanguage\Compiler;
use Symfony\Component\ExpressionLanguage\Node\Node;

class IdentityMonad implements MonadInterface {

  public function evaluate(Node $node, array $functions, array $values) {
    return $node->evaluate($functions, $values);
  }

  public function compile(Compiler $compiler, Node $node) {
    return $compiler->compile($node);
  }

  public function validateAst(Node $node, array $functions, array $names) {
  }

}
