<?php

namespace Drupal\expression\MonadicExpressionLanguage;

use Symfony\Component\ExpressionLanguage\Compiler;
use Symfony\Component\ExpressionLanguage\Node\Node;

class PrecompiledNode extends Node {

  public function __construct($compiled) {
    parent::__construct([], ['compiled' => $compiled]);
  }

  public function compile(Compiler $compiler) {
    $compiler->raw($this->attributes['compiled']);
  }

}
