<?php

namespace Drupal\expression\Dumper;

use Drupal\expression\Sandboxing\Sandboxed\SandboxedInterface;
use Kint\Object\BasicObject;
use Kint\Object\MethodObject;
use Kint\Object\Representation\Representation;
use Kint\Parser\ClassMethodsPlugin;

class ExpressionClassMethodsPlugin extends ClassMethodsPlugin {

  public function parse(&$var, BasicObject &$o, $trigger) {
    // Only show methods from sandboxed objects not starting with '_'.
    if ($var instanceof SandboxedInterface) {
      parent::parse($var, $o, $trigger);
      $methods = $o->getRepresentation('methods');
      assert($methods instanceof Representation);
      $methods->contents = array_values(array_filter($methods->contents, function (MethodObject $method) {
        return substr($method->getName(), 0, 1) !== '_'
          && !$method->static;
      }));
    }
  }

}
