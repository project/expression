<?php

namespace Drupal\expression\Dumper;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;
use Kint\Kint;
use Kint\Object\BasicObject;
use Kint\Parser\BlacklistPlugin;

class Dumper implements DumperInterface {

  use StringTranslationTrait;

  public function dump(MessengerInterface $messenger, $data, string $label, int $dumpDepth): void {
    // Taken from \Drupal\kint\Plugin\Devel\Dumper\Kint::export,
    // but we use upstream kint, not the devel fork.
    $kint = $this->createKint($dumpDepth);
    $dump = $kint->dumpAll([$data], [BasicObject::blank($label, '')]);

    $messenger->addWarning($this->t('Find expression dumps at the bottom.'));
    $messenger->addWarning(Markup::create($dump));
  }

  private function createKint($max_depth): Kint {
    $statics = [
      'aliases' => [],
      'app_root_dirs' => ['drupalRoot' => \Drupal::root()],
      'cli_detection' => TRUE,
      'display_called_from' => FALSE,
      'enabled_mode' => TRUE,
      'expanded' => FALSE,
      'file_link_format' => '',
      'max_depth' => $max_depth,
      'mode_default' => Kint::MODE_RICH,
      'mode_default_cli' => Kint::MODE_CLI,
      'plugins' => [
        'Kint\\Parser\\ArrayObjectPlugin',
        'Kint\\Parser\\Base64Plugin',
        'Kint\\Parser\\BlacklistPlugin',
        // 'Kint\\Parser\\ClassMethodsPlugin',
        'Drupal\expression\Dumper\ExpressionClassMethodsPlugin',
        // 'Kint\\Parser\\ClassStaticsPlugin',
        'Kint\\Parser\\ClosurePlugin',
        'Kint\\Parser\\ColorPlugin',
        'Kint\\Parser\\DateTimePlugin',
        'Kint\\Parser\\FsPathPlugin',
        'Kint\\Parser\\IteratorPlugin',
        'Kint\\Parser\\JsonPlugin',
        'Kint\\Parser\\MicrotimePlugin',
        'Kint\\Parser\\SimpleXMLElementPlugin',
        'Kint\\Parser\\SplFileInfoPlugin',
        'Kint\\Parser\\SplObjectStoragePlugin',
        'Kint\\Parser\\StreamPlugin',
        'Kint\\Parser\\TablePlugin',
        'Kint\\Parser\\ThrowablePlugin',
        'Kint\\Parser\\TimestampPlugin',
        'Kint\\Parser\\TracePlugin',
        'Kint\\Parser\\XmlPlugin',
      ],
      'renderers' => [
        Kint::MODE_RICH => 'Kint\\Renderer\\RichRenderer',
        Kint::MODE_PLAIN => 'Kint\\Renderer\\PlainRenderer',
        Kint::MODE_TEXT => 'Kint\\Renderer\\TextRenderer',
        Kint::MODE_CLI => 'Kint\\Renderer\\CliRenderer',
      ],
      'return' => TRUE,
    ];
    $kint = Kint::createFromStatics($statics);
    // Dunno why, copied from Kint::dump.
    $kint->setStatesFromStatics($statics);
    BlacklistPlugin::$shallow_blacklist[] = PluginManagerInterface::class;
    BlacklistPlugin::$shallow_blacklist[] = TranslatorInterface::class;
    return $kint;
  }

}
