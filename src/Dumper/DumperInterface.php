<?php

namespace Drupal\expression\Dumper;

use Drupal\Core\Messenger\MessengerInterface;

interface DumperInterface {

  public function dump(MessengerInterface $messenger, $data, string $label, int $dumpDepth): void;

}
