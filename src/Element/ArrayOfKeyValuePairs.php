<?php

namespace Drupal\expression\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Annotation\FormElement;

/**
 * Provides a form element for input of an array of lines in a textarea.
 *
 * Properties:
 *  - #key_value_separator: The character separating key and value.
 *  - #key_pattern: The preg pattern to validate key against.
 *  - #value_pattern: The preg pattern to validate value against.
 *
 * Textarea properties:
 * - #rows: Number of rows in the text box.
 * - #cols: Number of columns in the text box.
 * - #resizable: Controls whether the text area is resizable.  Allowed values
 *   are "none", "vertical", "horizontal", or "both" (defaults to "vertical").
 * - #maxlength: The maximum amount of characters to accept as input.
 *
 * Usage example:
 * @code
 * $form['text'] = array(
 *   '#type' => 'expression_array_of_key_value_pairs',
 *   '#title' => $this->t('Key value pairs'),
 * );
 * @endcode
 *
 * @FormElement("expression_array_of_key_value_pairs")
 */
class ArrayOfKeyValuePairs extends ArrayOfLines {

  public function getInfo($class = NULL) {
    $class = $class ?? static::class;
    $info = parent::getInfo($class);
    unset($info['#line_pattern']);
    $info['#key_value_separator'] = '=';
    $info['#key_pattern'] = NULL;
    $info['#value_pattern'] = NULL;
    return $info;
  }

  protected static function addArrayOfLinesTextarea(&$element, $keyValuePairs) {
    $separator = $element['#key_value_separator'];
    $lines = [];
    foreach ($keyValuePairs as $key => $value) {
      $lines[] = "$key$separator$value";
    }
    parent::addArrayOfLinesTextarea($element, $lines);
  }

  public static function extractValueInAfterBuild($element) {
    $lines = parent::extractValueInAfterBuild($element);
    $separator = $element['#key_value_separator'] ?? '|';
    $keyValuePairs = [];
    foreach ($lines as $line) {
      [$key, $value] = explode($separator, $line, 2) + [1 => NULL];
      $keyValuePairs[$key] = $value;
    }
    return $keyValuePairs;
  }

  public static function validateLinePattern(&$element, FormStateInterface $form_state, &$complete_form) {
    $separator = $element['#key_value_separator'] ?? '|';
    $keyPattern = $element['#key_pattern'] ?? ".*";
    $keyPreg = '{^(?:' . $keyPattern . ')$}';
    $valuePattern = $element['#value_pattern'] ?? ".*";
    $valuePreg = '{^(?:' . $valuePattern . ')$}';

    $expressions = $form_state->getValue($element['#parents'], []);
    foreach ($expressions as $key => $value) {
      $tArgs = ['%name' => $element['#title'], '@key' => $key, '@separator' => $separator];
      if ($value === NULL) {
        $form_state->setError($element, t('%name field line "@key" lacks "@separator" separator.', $tArgs));
      }
      if (!preg_match($keyPreg, $key)) {
        $form_state->setError($element, t('%name field line "@key" has invalid key.', $tArgs));
      }
      if (!preg_match($valuePreg, $value)) {
        $form_state->setError($element, t('%name field line "@key" has invalid value.', $tArgs));
      }

    }
  }

}
