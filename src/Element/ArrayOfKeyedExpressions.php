<?php

namespace Drupal\expression\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\ContextInterface;
use Drupal\Core\Render\Annotation\FormElement;
use Drupal\expression\Context\ContextBagInterface;
use Drupal\expression\Context\UpdatableContextInterface;
use Drupal\expression\Expression\ExpressionListServiceInterface;
use Drupal\expression\Expression\Variables\VariableStorageInterface;

/**
 * Provides a form element for input of an array of lines in a textarea.
 *
 * Properties:
 *  - #expression_list_service: The expression list service with no logger,
 *    so validation can see what it throws.
 *  - #expression_list_service_names: The variable names.
 *
 * Array-of-keys properties:
 *  - #key_value_separator: The character separating key and value.
 *  - #key_pattern: The preg pattern to validate key against.
 *  - #value_pattern: The preg pattern to validate value against.
 *
 * Textarea properties:
 * - #rows: Number of rows in the text box.
 * - #cols: Number of columns in the text box.
 * - #resizable: Controls whether the text area is resizable.  Allowed values
 *   are "none", "vertical", "horizontal", or "both" (defaults to "vertical").
 * - #maxlength: The maximum amount of characters to accept as input.
 *
 * Usage example:
 * @code
 * $form['text'] = array(
 *   '#type' => 'expression_array_of_keyed_expressions',
 *   '#title' => $this->t('Expressions'),
 * );
 * @endcode
 *
 * @see \Drupal\Core\Render\Element\Textfield
 * @see \Drupal\filter\Element\TextFormat
 *
 * @FormElement("expression_array_of_keyed_expressions")
 */
class ArrayOfKeyedExpressions extends ArrayOfKeyValuePairs {

  public function getInfo($class = NULL) {
    $class = $class ?? static::class;
    $info = parent::getInfo($class);
    $info['#description'] = $this->t('Add lines like "name=expression". Use variables listed below. To define new variables, start them with a "_".');
    $info['#key_pattern'] = '[a-z_][a-z0-9_]*';
    $info['#expression_list_service'] = NULL;
    $info['#expression_variables'] = NULL;
    return $info;
  }

  public static function validateLinePattern(&$element, FormStateInterface $form_state, &$complete_form) {
    parent::validateLinePattern($element, $form_state, $complete_form);
    $expressions = $form_state->getValue($element['#parents'], []);
    $expressionListService = $element['#expression_list_service'] ?? NULL;
    $variables = $element['#expression_variables'];
    if (
      $expressionListService instanceof ExpressionListServiceInterface
      && $variables instanceof ContextBagInterface
    ) {
      foreach ($expressionListService->validate($expressions, $variables) as $error) {
        $form_state->setError($element, t('%name field: @error', ['%name' => $element['#title'], '@error' => $error]));
      }
    }
    else {
      throw new \LogicException("Element configured wrong.");
    }
  }

  protected static function addArrayOfLinesTextarea(&$element, $keyValuePairs) {
    parent::addArrayOfLinesTextarea($element, $keyValuePairs);
    $variables = $element['#expression_variables'];
    if ($variables instanceof ContextBagInterface) {
      $items = [];
      foreach ($variables->getContexts() as $contextName => $context) {
        assert($context instanceof ContextInterface);
        $tArgs = [
          '%variable' => $contextName,
          '@description' => $context->getContextDefinition()->getLabel(),
        ];
        $isReadonly = !$variables->isUpdatable($contextName);
        $varDescription = $isReadonly
          ? t('%variable (readonly): @description', $tArgs, ['context' => 'expression'])
          : t('%variable: @description', $tArgs, ['context' => 'expression']);
        $items[$contextName] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $varDescription,
        ];
      }
      $element['variables'] = [
        '#type' => 'details',
        '#title' => t('Variables'),
        'variables' => [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $items,
        ],
      ];
    }
  }


}
