<?php

namespace Drupal\expression\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a form element for input of an array of lines in a textarea.
 *
 * Properties:
 *  - #line_pattern: A preg pattern to validate each line.
 *  - #as_keys: Store lines as keys, not values.
 *  - #unique: Remove duplicates.
 *
 * Textarea properties:
 * - #rows: Number of rows in the text box.
 * - #cols: Number of columns in the text box.
 * - #resizable: Controls whether the text area is resizable.  Allowed values
 *   are "none", "vertical", "horizontal", or "both" (defaults to "vertical").
 * - #maxlength: The maximum amount of characters to accept as input.
 *
 * Usage example:
 * @code
 * $form['text'] = array(
 *   '#type' => 'expression_array_of_lines',
 *   '#title' => $this->t('Config lines'),
 * );
 * @endcode
 *
 * @FormElement("expression_array_of_lines")
 */
class ArrayOfLines extends FormElement{

  public function getInfo($class = NULL) {
    $class = $class ?? static::class;
    return [
      '#input' => TRUE,
      '#line_pattern' => NULL,
      '#as_keys' => FALSE,
      '#unique' => FALSE,
      '#process' => [
        [$class, 'processArrayOfLines']
      ],
      '#after_build' => [
        [$class, 'afterBuildArrayOfLines']
      ],
      '#element_validate' => [
        [$class, 'validateLinePattern']
      ],
    ];
  }

  public static function processArrayOfLines(&$element, FormStateInterface $form_state, &$complete_form) {
    $lines = $element['#default_value'];
    if ($element['#as_keys']) {
      $lines = array_keys($lines);
    }
    static::addArrayOfLinesTextarea($element, $lines);
    return $element;
  }

  protected static function addArrayOfLinesTextarea(&$element, $lines) {
    $defaultValue = implode("\n", $lines);
    $element['text'] = [
        '#type' => 'textarea',
        '#parents' => array_merge($element['#parents'], ['text']),
        '#default_value' => $defaultValue,
      ] + array_intersect_key($element, array_flip(['#title', '#description', '#cols', '#rows', '#resizable']));
  }

  public static function afterBuildArrayOfLines($element, FormStateInterface $formState) {
    $value = static::extractValueInAfterBuild($element);
    $element['#value'] = $value;
    $formState->setValue($element['#parents'], $value);
    return $element;
  }

  public static function extractValueInAfterBuild($element) {
    $textValue = $element['text']['#value'];
    preg_replace('/\r\n/', "\n", $textValue);
    $lines = explode("\n", $textValue);
    $lines = array_map('trim', $lines);
    $lines = array_filter($lines);
    if ($element['#unique']) {
      $lines = array_unique($lines);
    }
    if ($element['#as_keys']) {
      $lines = array_fill_keys($lines, TRUE);
    }
    return $lines;
  }

  public static function validateLinePattern(&$element, FormStateInterface $form_state, &$complete_form) {
    // @see \Drupal\Core\Render\Element\FormElement::validatePattern
    $linePattern = $element['#line_pattern'];
    if ($linePattern) {
      $preg = '{^(?:' . $linePattern . ')$}';
      $lines = $form_state->getValue($element['#parents'], []);
      if ($element['#as_keys']) {
        $lines = array_keys($lines);
      }
      foreach ($lines as $i => $line) {
        if (!preg_match($preg, $line)) {
          $form_state->setError($element, t('%name field line @line is not in the right format.',
            ['%name' => $element['#title'], '@line' => $i + 1]));
        }
      }
    }
  }

}
