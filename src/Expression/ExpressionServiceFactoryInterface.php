<?php

namespace Drupal\expression\Expression;

use Psr\Log\LoggerInterface;

interface ExpressionServiceFactoryInterface {

  /**
   * @param string $forComponent
   *   An arbitrary name of the component using the instance, to allow
   *   decorators to decide what and how to alter.
   * @param \Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface[] $functionProviders
   *   The function providers.
   * @param \Psr\Log\LoggerInterface|null $logger
   *   If a logger is given, exceptions will be logged, not thrown.
   *
   * @return \Drupal\expression\Expression\ExpressionService
   */
  public function createInstance(string $forComponent, array $functionProviders, ?LoggerInterface $logger): ExpressionService;

}
