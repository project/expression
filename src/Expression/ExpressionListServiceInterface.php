<?php

namespace Drupal\expression\Expression;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\expression\Context\ContextBagInterface;

interface ExpressionListServiceInterface {

  /**
   * Validate an expression list.
   *
   * @param \Symfony\Component\ExpressionLanguage\Expression[]|string[] $expressions
   *   The expression to evaluate, keyed by variable name to assign.
   * @param \Drupal\expression\Context\ContextBagInterface $contextBag
   *   The variable values.
   *
   * @throws \Symfony\Component\ExpressionLanguage\SyntaxError
   */
  public function validate(array $expressions, ContextBagInterface $contextBag): array;

  /**
   * Evaluate an expression list.
   *
   * @param \Symfony\Component\ExpressionLanguage\Expression[]|string[] $expressions
   *   The expression to evaluate, keyed by variable name to assign.
   * @param \Drupal\expression\Context\ContextBagInterface $contextBag
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $expressionsCacheableDependency
   *   The variable values.
   *
   * @return \Drupal\expression\Context\ContextBagInterface
   *   The updated variable values.
   */
  public function evaluate(array $expressions, ContextBagInterface $contextBag, CacheableDependencyInterface $expressionsCacheableDependency): ContextBagInterface;

}
