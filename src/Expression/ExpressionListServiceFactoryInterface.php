<?php

namespace Drupal\expression\Expression;

use Drupal\Core\Messenger\MessengerInterface;
use Psr\Log\LoggerInterface;

interface ExpressionListServiceFactoryInterface {

  /**
   * @param string $componentName
   *   An arbitrary name of the component using the instance, to allow
   *   decorators to decide what and how to alter.
   * @param \Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface[] $functionProviders
   *   The function providers.
   * @param \Psr\Log\LoggerInterface|null $errorLogger
   *   If a logger is given, exceptions will be logged, not thrown.
   * @param \Drupal\Core\Messenger\MessengerInterface|null $debugMessenger
   * @param string $debugLabel
   * @param string $dumpCondition
   *
   * @return \Drupal\expression\Expression\ExpressionListServiceInterface
   */
  public function createInstance(string $componentName, array $functionProviders, ?LoggerInterface $errorLogger = NULL, ?MessengerInterface $debugMessenger = NULL, string $debugLabel = 'Expressions', $dumpCondition = 'dump'): ExpressionListServiceInterface;

}
