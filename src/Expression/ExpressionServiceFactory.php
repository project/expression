<?php

namespace Drupal\expression\Expression;

use Drupal\expression\MonadicExpressionLanguage\CacheabilityMonad\CacheabilityMonad;
use Drupal\expression\MonadicExpressionLanguage\MonadicExpressionLanguage;
use Drupal\expression\Sandboxing\SandboxerInterface;
use Psr\Log\LoggerInterface;

final class ExpressionServiceFactory implements ExpressionServiceFactoryInterface {

  /**
   * @var \Drupal\expression\Sandboxing\SandboxerInterface
   */
  private $sandboxer;

  public function __construct(SandboxerInterface $sandboxer) {
    $this->sandboxer = $sandboxer;
  }

  public function createInstance(string $forComponent, array $functionProviders, ?LoggerInterface $logger): ExpressionService {
    return new ExpressionService(
      new MonadicExpressionLanguage(
        NULL, $functionProviders, new CacheabilityMonad($this->sandboxer)
      ), $logger);
  }

}
