<?php

namespace Drupal\expression\Expression;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\expression\Dumper\DumperInterface;
use Psr\Log\LoggerInterface;

class ExpressionListServiceFactory implements ExpressionListServiceFactoryInterface {

  /**
   * @var \Drupal\expression\Expression\ExpressionServiceFactoryInterface
   */
  protected $expressionServiceFactory;

  /**
   * @var \Drupal\expression\Dumper\DumperInterface
   */
  protected $dumper;

  public function __construct(ExpressionServiceFactoryInterface $expressionServiceFactory, DumperInterface $dumper) {
    $this->expressionServiceFactory = $expressionServiceFactory;
    $this->dumper = $dumper;
  }


  public function createInstance(string $componentName, array $functionProviders, ?LoggerInterface $errorLogger = NULL, ?MessengerInterface $debugMessenger = NULL, string $debugLabel = 'Expressions', $dumpCondition = 'dump'): ExpressionListServiceInterface {
    return new ExpressionListService(
      $this->expressionServiceFactory->createInstance(
        $componentName, $functionProviders, $errorLogger
      ), $errorLogger, $debugMessenger, $debugLabel, $this->dumper, $dumpCondition);
  }

}
