<?php

namespace Drupal\expression\Expression;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\ContextInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\expression\Context\ContextBagInterface;
use Drupal\expression\Context\ContextTool;
use Drupal\expression\Context\UpdatableContext;
use Drupal\expression\Context\UpdatableContextInterface;
use Drupal\expression\Dumper\DumperInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ExpressionListService
 *
 * @todo Declare some classes internal.
 * @todo Check order of sandboxer interface checks.
 * @todo Create Sandboxed ContextBag.
 * @todo Create ContextBagBuilder.
 * @todo Write docs.
 */
class ExpressionListService implements ExpressionListServiceInterface {

  use StringTranslationTrait;
  /**
   * @var \Drupal\expression\Expression\ExpressionServiceInterface
   */
  protected $expressionService;

  /**
   * @var \Psr\Log\LoggerInterface|null
   */
  protected $errorLogger;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface|null
   */
  protected $debugMessenger;

  /**
   * @var string
   */
  protected $sanitizedDebugLabel;

  /**
   * @var \Drupal\expression\Dumper\DumperInterface
   */
  protected $dumper;

  /**
   * @var bool|string
   */
  protected $dumpCondition;

  /**
   * ExpressionListService constructor.
   *
   * @internal Use ExpressionListServiceFactory
   *
   * @param \Drupal\expression\Expression\ExpressionServiceInterface $expressionService
   * @param \Psr\Log\LoggerInterface|null $errorLogger
   * @param \Drupal\Core\Messenger\MessengerInterface|null $debugMessenger
   * @param string $debugLabel
   * @param \Drupal\expression\Dumper\DumperInterface $dumper
   * @param bool|string $dumpCondition
   */
  public function __construct(ExpressionServiceInterface $expressionService, ?LoggerInterface $errorLogger, ?MessengerInterface $debugMessenger, string $debugLabel, DumperInterface $dumper, $dumpCondition) {
    $this->expressionService = $expressionService;
    $this->errorLogger = $errorLogger;
    $this->debugMessenger = $debugMessenger;
    $this->sanitizedDebugLabel = Html::escape($debugLabel);
    $this->dumper = $dumper;
    $this->dumpCondition = $dumpCondition;
  }

  public function validate(array $expressions, ContextBagInterface $contextBag): array {
    $errors = [];
    foreach ($expressions as $varName => $expression) {
      $error = $this->expressionService->validate($expression, $contextBag);
      if ($error) {
        $errors[] = "$varName: $error";
      }
      if (!$contextBag->hasContext($varName)) {
        if (substr($varName, 0, 1) === '_') {
          $contextBag->addContext($varName, new Context(ContextDefinition::create()));
        }
        else {
          $errors[] = "Can not create new variable without leading underscore: $varName";
        }
      }
    }
    return $errors;
  }

  public function evaluate(array $expressions, ContextBagInterface $contextBag, CacheableDependencyInterface $expressionsCacheableDependency): ContextBagInterface {
    foreach ($expressions as $varName => $expression) {
      try {
        $result = $this->expressionService->evaluate($expression, $contextBag);
        if (!$contextBag->hasContext($varName)) {
          if (substr($varName, 0, 1) === '_') {
            $contextBag->addUpdatableContext($varName, new Context(ContextDefinition::create()->setRequired(FALSE)));
          }
          else {
            throw new \RuntimeException("Can not create new variable without leading underscore: $varName");
          }
        }
        $contextBag = $contextBag->createUpdatedBag($varName, $result);
        $contextBag->addCacheableDependencyToUpdatables($expressionsCacheableDependency);
      }
      catch (\Throwable $e) {
        if ($this->errorLogger) {
          $logMessage = "<pre>{$e->getMessage()}\n{$e->getTraceAsString()}</pre>";
          $this->errorLogger->error($logMessage);
        }
        else {
          throw $e;
        }
        if ($this->debugMessenger) {
          $messengerMessage = Markup::create("<details><summary>Expression $this->sanitizedDebugLabel:$varName: {$e->getMessage()}</summary><pre>{$e->getTraceAsString()}</pre></details>");
          $this->debugMessenger->addError($messengerMessage);
        }
      }
    }

    $dumpInfo = (
      is_string($this->dumpCondition)
      && ($dumpContext = $contextBag->getContext($this->dumpCondition))
    )
      ? $dumpContext->getContextValue() : $this->dumpCondition;
    if (is_bool($dumpInfo)) {
      $mustDump = $dumpInfo;
      $dumpDepth = 6;
    }
    elseif (is_numeric($dumpInfo)) {
      $mustDump = TRUE;
      $dumpDepth = (int)$dumpInfo;
    }
    else {
      $mustDump = FALSE;
      $dumpDepth = 6;
    }

    if ($mustDump) {
      $data = ContextTool::getSandboxedValues($contextBag->getContexts());
      // @todo Improve displaying cacheabilities.
      $data['+cacheabilities'] = array_map(function (ContextInterface $context) {
        return (new CacheableMetadata())->addCacheableDependency($context);
      }, $contextBag->getContexts());
      $this->dumper->dump($this->debugMessenger, $data, $this->sanitizedDebugLabel ?: 'Expression variables', $dumpDepth);
    }

    return $contextBag;
  }


}
