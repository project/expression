<?php

namespace Drupal\expression\Expression;

use Drupal\expression\Context\ContextBagInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\SyntaxError;

class ExpressionService implements ExpressionServiceInterface {

  /**
   * @var \Symfony\Component\ExpressionLanguage\ExpressionLanguage
   */
  protected $expressionLanguage;

  /**
   * @var \Psr\Log\LoggerInterface|null
   */
  protected $logger;

  /**
   * ExpressionService constructor.
   *
   * @param \Symfony\Component\ExpressionLanguage\ExpressionLanguage $expressionLanguage
   * @param \Psr\Log\LoggerInterface|null $logger
   */
  public function __construct(ExpressionLanguage $expressionLanguage, ?LoggerInterface $logger) {
    $this->expressionLanguage = $expressionLanguage;
    $this->logger = $logger;
  }

  public function validate($expression, ContextBagInterface $contextBag) {
    $names = array_keys($contextBag->getContexts());
    try {
      $this->expressionLanguage->parse($expression, $names);
    } catch (SyntaxError $e) {
      return $e->getMessage();
    }
    return NULL;
  }

  public function evaluate($expression, ContextBagInterface $contextBag) {
    try {
      $values = $contextBag->getContexts();
      $result = $this->expressionLanguage->evaluate($expression, $values);
      return $result;
    }
    catch (SyntaxError $e) {
      if ($this->logger) {
        $this->logger->error($e->getMessage());
        return NULL;
      }
      else {
        throw $e;
      }
    }
  }

}
