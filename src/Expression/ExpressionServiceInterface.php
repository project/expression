<?php

namespace Drupal\expression\Expression;

use Drupal\expression\Context\ContextBagInterface;

interface ExpressionServiceInterface {

  /**
   * Validate an expression.
   *
   * @param \Symfony\Component\ExpressionLanguage\Expression|string $expression
   *   The expression to evaluate.
   * @param \Drupal\expression\Context\ContextBagInterface $variables
   *   The variable values.
   *
   * @return ?string
   *   The validation error, or NULL.
   */
  public function validate($expression, ContextBagInterface $contextBag);

  /**
   * Evaluate an expression.
   *
   * @param \Symfony\Component\ExpressionLanguage\Expression|string $expression
   *   The expression to evaluate.
   * @param \Drupal\expression\Context\ContextBagInterface $variables
   *   The variable values.
   *
   * @return mixed
   *   The result of the evaluation of the expression.
   */
  public function evaluate($expression, ContextBagInterface $contextBag);

}
