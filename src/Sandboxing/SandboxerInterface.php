<?php

namespace Drupal\expression\Sandboxing;

interface SandboxerInterface {

  public function wrap($item);

}
