<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;

final class SandboxedConfigEntityType extends SandboxedWrapperBase {

  public function __construct(ConfigEntityTypeInterface $wrapped) {
    $this->wrapped = $wrapped;
  }

  use SandboxedConfigEntityTypeTrait;

}
