<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

trait SandboxedTypedDataTrait {

  /**
   * @var \Drupal\Core\TypedData\TypedDataInterface
   */
  protected $wrapped;

  public function getValue() {
    return $this->wrapped->getValue();
  }

  public function getString() {
    return $this->wrapped->getString();
  }

  public function getName() {
    return $this->wrapped->getName();
  }

  public function getParent() {
    return $this->wrapped->getParent();
  }

  public function getRoot() {
    return $this->wrapped->getRoot();
  }

  public function getPropertyPath() {
    return $this->wrapped->getPropertyPath();
  }

}
