<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

trait SandboxedComplexDataTrait {

  use SandboxedTypedDataTrait;

  /**
   * @var \Drupal\Core\TypedData\ComplexDataInterface
   */
  protected $wrapped;

  public function get($property_name) {
    return $this->wrapped->get($property_name);
  }

  public function getProperties($include_computed = FALSE) {
    return $this->wrapped->getProperties($include_computed);
  }

  public function toArray() {
    return $this->wrapped->toArray();
  }

  public function isEmpty() {
    return $this->wrapped->isEmpty();
  }

}
