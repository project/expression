<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Entity\ContentEntityInterface;

final class SandboxedContentEntity extends SandboxedWrapperBase {

  use SandboxedContentEntityTrait;

  public function __construct(ContentEntityInterface $wrapped) {
    $this->wrapped = $wrapped;
  }

}
