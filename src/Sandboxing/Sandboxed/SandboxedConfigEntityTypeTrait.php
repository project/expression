<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

trait SandboxedConfigEntityTypeTrait {

  use SandboxedEntityTypeTrait;

  /**
   * @var \Drupal\Core\Config\Entity\ConfigEntityTypeInterface
   */
  protected $wrapped;

  public function getConfigPrefix() {
    $result = $this->wrapped->getConfigPrefix();
    return $result;
  }

}
