<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Cache\CacheableMetadata;

/**
 * A sandboxed object.
 *
 * This marker interface carries the contract that the object wrapper
 * implementing it is audited for these guarantees:
 * - NoSideEffects: All methods are getters (so do not change any state)
 * - NoLostCacheability: All method results implement
 *   CacheableDependencyInterface, lest they only carry cacheability of method's
 *   object.
 * - NoEscape: All method results can be handled by sandboxer.
 */
interface SandboxedWrapperInterface extends SandboxedInterface {

  public function _getWrapped();

  public function _getCacheability(): ?CacheableMetadata;

}
