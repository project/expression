<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

/**
 * Class SandboxedConfigEntity
 *
 * Audit:
 * - NoSideEffects: ✅
 * - NoLostCacheability: ✅
 * - NoEscape: ✅
 */
trait SandboxedConfigEntityTrait {

  use SandoxedEntityTrait;

  /**
   * @var \Drupal\Core\Config\Entity\ConfigEntityInterface
   */
  protected $wrapped;

  public function get($property_name) {
    return $this->wrapped->get($property_name);
  }

  public function getDependencies() {
    return $this->wrapped->getDependencies();
  }

  public function isInstallable() {
    return $this->wrapped->isInstallable();
  }

  public function isSyncing() {
    return $this->wrapped->isSyncing();
  }

}
