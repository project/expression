<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\language\ConfigurableLanguageInterface;

final class SandboxedConfigurableLanguage extends SandboxedWrapperBase {

  use SandboxedConfigEntityTrait;
  use SandboxedLanguageTrait;

  public function __construct(ConfigurableLanguageInterface $wrapped) {
    $this->wrapped = $wrapped;
  }

}
