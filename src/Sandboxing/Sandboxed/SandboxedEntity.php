<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedWrapperInterface;
use Drupal\expression\Sandboxing\SandboxerInterface;

/**
 * Class SandboxedEntity
 *
 * Audit:
 * - NoSideEffects: ✅
 * - NoLostCacheability: ✅
 *   - @fixme toUrl
 *   - @fixme toLink
 *   - accessResult:
 *     ✅ AccessResult implements CacheableDependencyInterface
 *   - language:
 *     ✅ ConfigurableLanguage implements CacheableDependencyInterface
 *     ✅ Language has no additional cacheability
 *   - @fixme getEntityType
 *     ?
 * - NoEscape:
 *   - @fixme toUrl
 *   - @fixme toLink
 *   - @fixme accessResult => AccessResult
 *   - @fixme language => Language, ConfigurableLanguage
 *   - @fixme getEntityTypem => EntityType, ContentEntityType, ConfigEntityType
 */
final class SandboxedEntity extends SandboxedWrapperBase {

  use SandoxedEntityTrait;

  public function __construct(EntityInterface $wrapped) {
    $this->wrapped = $wrapped;
  }

}
