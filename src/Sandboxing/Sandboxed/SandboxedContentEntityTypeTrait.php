<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

trait SandboxedContentEntityTypeTrait {

  use SandboxedEntityTypeTrait;

  /**
   * @var \Drupal\Core\Entity\ContentEntityTypeInterface
   */
  protected $wrapped;

  public function getRevisionMetadataKeys($include_backwards_compatibility_field_names = TRUE) {
    $result = $this->wrapped->getRevisionMetadataKeys($include_backwards_compatibility_field_names);
    return $result;
  }

  public function getRevisionMetadataKey($key) {
    $result = $this->wrapped->getRevisionMetadataKey($key);
    return $result;
  }

}
