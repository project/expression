<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Entity\EntityTypeInterface;

final class SandboxedEntityType extends SandboxedWrapperBase {

  use SandboxedEntityTypeTrait;

  public function __construct(EntityTypeInterface $wrapped) {
    $this->wrapped = $wrapped;
  }

}
