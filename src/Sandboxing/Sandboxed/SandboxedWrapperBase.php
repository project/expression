<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\expression\Sandboxing\SandboxerInterface;

abstract class SandboxedWrapperBase implements SandboxedWrapperInterface {

  /**
   * @var object
   */
  protected $wrapped;

  public function _getWrapped() {
    return $this->wrapped;
  }

  public function _getCacheability(): ?CacheableMetadata {
    if ($this->wrapped instanceof CacheableDependencyInterface) {
      return CacheableMetadata::createFromObject($this->wrapped);
    }
    return NULL;
  }

}
