<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

trait SandboxedListTrait {

  use SandboxedTypedDataTrait;

  /**
   * @var \Drupal\Core\TypedData\ListInterface
   */
  protected $wrapped;

  public function isEmpty() {
    return $this->wrapped->isEmpty();
  }

  public function get($index) {
    return $this->wrapped->get($index);
  }

  public function first() {
    return $this->wrapped->first();
  }

}
