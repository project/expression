<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

/**
 * Class SandboxedContentEntity
 *
 * Audit:
 * - NoSideEffects: ✅
 * - NoLostCacheability: @fixme
 * - NoEscape:
 *   - @fixme get
 *     FieldItemListInterface
 *   - @fixme getFields
 *     FieldItemListInterface[]
 *   - @fixme getTranslationLanguages
 *     Language[] | ConfigurableLanguage[]
 *   - @fixme __get/__isset
 *     FieldItemListInterface
 */
trait SandboxedContentEntityTrait {

  use SandoxedEntityTrait;

  /**
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $wrapped;

  public function getLoadedRevisionId() {
    return $this->wrapped->getLoadedRevisionId();
  }

  public function isNewRevision() {
    return $this->wrapped->isNewRevision();
  }

  public function isDefaultRevision($new_value = NULL) {
    return $this->wrapped->isDefaultRevision($new_value);
  }

  public function wasDefaultRevision() {
    return $this->wrapped->wasDefaultRevision();
  }

  public function isLatestRevision() {
    return $this->wrapped->isLatestRevision();
  }

  public function isLatestTranslationAffectedRevision() {
    return $this->wrapped->isLatestTranslationAffectedRevision();
  }

  public function isRevisionTranslationAffected() {
    return $this->wrapped->isRevisionTranslationAffected();
  }

  public function isDefaultTranslation() {
    return $this->wrapped->isDefaultTranslation();
  }

  public function getRevisionId() {
    return $this->wrapped->getRevisionId();
  }

  public function isTranslatable() {
    return $this->wrapped->isTranslatable();
  }

  public function hasField($field_name) {
    return $this->wrapped->hasField($field_name);
  }

  public function get($field_name) {
    return $this->wrapped->get($field_name);
  }

  public function getFields($include_computed = TRUE) {
    return $this->wrapped->getFields($include_computed);
  }

  public function getTranslation($langcode) {
    return $this->wrapped->getTranslation($langcode);
  }

  public function getUntranslated() {
    return $this->wrapped->getUntranslated();
  }

  public function hasTranslation($langcode) {
    return $this->wrapped->hasTranslation($langcode);
  }

  public function isNewTranslation() {
    return $this->wrapped->isNewTranslation();
  }

  public function getTranslationLanguages($include_default = TRUE) {
    return $this->wrapped->getTranslationLanguages($include_default);
  }

  public function hasTranslationChanges() {
    return $this->wrapped->hasTranslationChanges();
  }

  public function isSyncing() {
    return $this->wrapped->isSyncing();
  }

  public function __get($name) {
    return $this->wrapped->__get($name);
  }

  public function __isset($name) {
    return $this->wrapped->__isset($name);
  }

}
