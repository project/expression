<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\TypedData\TypedDataInterface;

class SandboxedTypedData {

  use SandboxedTypedDataTrait;

  public function __construct(TypedDataInterface $wrapped) {
    $this->wrapped = $wrapped;
  }

}
