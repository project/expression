<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

trait SandboxedLanguageTrait {

  /**
   * @var \Drupal\Core\Language\LanguageInterface
   */
  protected $wrapped;

  public function getName() {
    return $this->wrapped->getName();
  }

  public function getId() {
    return $this->wrapped->getId();
  }

  public function getDirection() {
    return $this->wrapped->getDirection();
  }

  public function getWeight() {
    return $this->wrapped->getWeight();
  }

  public function isDefault() {
    return $this->wrapped->isDefault();
  }

  public function isLocked() {
    return $this->wrapped->isLocked();
  }

}
