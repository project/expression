<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Session\AccountInterface;

trait SandboxedAccessibleTrait {

  /**
   * @var \Drupal\Core\Access\AccessibleInterface
   */
  protected $wrapped;

  public function access($operation, AccountInterface $account = NULL) {
    return $this->wrapped->access($operation, $account, TRUE);
  }

}
