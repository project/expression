<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Language\LanguageInterface;

final class SandboxedLanguage extends SandboxedWrapperBase {

  use SandboxedLanguageTrait;

  public function __construct(LanguageInterface $wrapped) {
    $this->wrapped = $wrapped;
  }

}
