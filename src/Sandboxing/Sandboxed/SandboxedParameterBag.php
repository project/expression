<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Symfony\Component\HttpFoundation\ParameterBag;

final class SandboxedParameterBag extends SandboxedWrapperBase {

  /**
   * @var \Symfony\Component\HttpFoundation\ParameterBag
   */
  protected $wrapped;

  public function __construct(ParameterBag $wrapped) {
    $this->wrapped = $wrapped;
  }

  public function all() {
    return $this->wrapped->all();
  }

  public function keys() {
    return $this->wrapped->keys();
  }

  public function get($key, $default = NULL) {
    return $this->wrapped->get($key, $default);
  }

  public function has($key) {
    return $this->wrapped->has($key);
  }

}
