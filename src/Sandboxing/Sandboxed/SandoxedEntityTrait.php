<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

trait SandoxedEntityTrait {

  use SandboxedAccessibleTrait;

  /**
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $wrapped;

  public function id() {
    return $this->wrapped->id();
  }

  public function isNew() {
    return $this->wrapped->isNew();
  }

  public function bundle() {
    return $this->wrapped->bundle();
  }

  public function label() {
    return $this->wrapped->label();
  }

  public function toUrl($rel = 'canonical', array $options = []) {
    return $this->wrapped->toUrl($rel, $options);
  }

  public function hasLinkTemplate($rel) {
    return $this->wrapped->hasLinkTemplate($rel);
  }

  public function toLink($text = NULL, $rel = 'canonical', array $options = []) {
    return $this->wrapped->toLink($text, $rel, $options);
  }

  public function uriRelationships() {
    return $this->wrapped->uriRelationships();
  }

  public function language() {
    return $this->wrapped->language();
  }

  public function getEntityType() {
    return $this->wrapped->getEntityType();
  }

  public function getEntityTypeId() {
    return $this->wrapped->getEntityTypeId();
  }

  public function getOriginalId() {
    return $this->wrapped->getOriginalId();
  }

  public function toArray() {
    return $this->wrapped->toArray();
  }

  public function getConfigDependencyName() {
    return $this->wrapped->getConfigDependencyName();
  }

  public function createDuplicate() {
    return $this->wrapped->createDuplicate();
  }

  public function getConfigTarget() {
    return $this->wrapped->getConfigTarget();
  }

}
