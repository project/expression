<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

trait SandboxedEntityTypeTrait {

  /**
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $wrapped;

  public function get($property) {
    return $this->wrapped->get($property);
  }

  public function getKeys() {
    return $this->wrapped->getKeys();
  }

  public function getKey($key) {
    return $this->wrapped->getKey($key);
  }

  public function hasKey($key) {
    return $this->wrapped->hasKey($key);
  }

  public function getBundleEntityType() {
    return $this->wrapped->getBundleEntityType();
  }

  public function getBundleOf() {
    return $this->wrapped->getBundleOf();
  }

  public function getBundleLabel() {
    return $this->wrapped->getBundleLabel();
  }

  public function isRevisionable() {
    return $this->wrapped->isRevisionable();
  }

  public function getLabel() {
    return $this->wrapped->getLabel();
  }

  public function getCollectionLabel() {
    return $this->wrapped->getCollectionLabel();
  }

  public function getSingularLabel() {
    return $this->wrapped->getSingularLabel();
  }

  public function getPluralLabel() {
    return $this->wrapped->getPluralLabel();
  }

  public function getCountLabel($count) {
    return $this->wrapped->getCountLabel($count);
  }

  public function getGroup() {
    return $this->wrapped->getGroup();
  }

}
