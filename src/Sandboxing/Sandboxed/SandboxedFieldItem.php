<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Field\FieldItemInterface;

final class SandboxedFieldItem extends SandboxedWrapperBase {

  use SandboxedComplexDataTrait;

  /**
   * @var \Drupal\Core\Field\FieldItemInterface
   */
  protected $wrapped;

  public function __construct(FieldItemInterface $wrapped) {
    $this->wrapped = $wrapped;
  }

  public function getEntity() {
    return $this->wrapped->getEntity();
  }

  public function getLangcode() {
    return $this->wrapped->getLangcode();
  }

  public function getFieldDefinition() {
    return $this->wrapped->getFieldDefinition();
  }

  public function __get($name) {
    return $this->wrapped->__get($name);
  }

  public function __isset($name) {
    return $this->wrapped->__isset($name);
  }

  public function view($display_options = []) {
    return $this->wrapped->view($display_options);
  }

}
