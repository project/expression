<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;

final class SandboxedFieldItemList extends SandboxedWrapperBase {

  use SandboxedAccessibleTrait;
  use SandboxedListTrait;

  /**
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  protected $wrapped;

  public function __construct(FieldItemListInterface $wrapped) {
    $this->wrapped = $wrapped;
  }

  public function getEntity() {
    return $this->wrapped->getEntity();
  }

  public function getLangcode() {
    return $this->wrapped->getLangcode();
  }

  public function getFieldDefinition() {
    return $this->wrapped->getFieldDefinition();
  }

  public function getSettings() {
    return $this->wrapped->getSettings();
  }

  public function getSetting($setting_name) {
    return $this->wrapped->getSetting($setting_name);
  }

  public function __get($property_name) {
    return $this->wrapped->__get($property_name);
  }

  public function __isset($property_name) {
    return $this->wrapped->__isset($property_name);
  }

  public function defaultAccess($operation = 'view', AccountInterface $account = NULL) {
    return $this->wrapped->defaultAccess($operation, $account);
  }

  public function view($display_options = []) {
    return $this->wrapped->view($display_options);
  }

  public function equals(FieldItemListInterface $list_to_compare) {
    return $this->wrapped->equals($list_to_compare);
  }

  public function hasAffectingChanges(FieldItemListInterface $original_items, $langcode) {
    return $this->wrapped->hasAffectingChanges($original_items, $langcode);
  }

}
