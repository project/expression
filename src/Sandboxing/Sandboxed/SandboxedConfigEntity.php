<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

final class SandboxedConfigEntity extends SandboxedWrapperBase {

  public function __construct(ConfigEntityInterface $wrapped) {
    $this->wrapped = $wrapped;
  }

  use SandboxedConfigEntityTrait;

}
