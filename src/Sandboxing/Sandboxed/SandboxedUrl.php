<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Session\AccountInterface;

final class SandboxedUrl extends SandboxedWrapperBase {

  /**
   * @var \Drupal\Core\Url
   */
  protected $wrapped;

  public function __construct(\Drupal\Core\Url $wrapped) {
    $this->wrapped = $wrapped;
  }

  public function toUriString() {
    return $this->wrapped->toUriString();
  }

  public function isExternal() {
    return $this->wrapped->isExternal();
  }

  public function isRouted() {
    return $this->wrapped->isRouted();
  }

  public function getRouteName() {
    return $this->wrapped->getRouteName();
  }

  public function getRouteParameters() {
    return $this->wrapped->getRouteParameters();
  }

  public function getOptions() {
    return $this->wrapped->getOptions();
  }

  public function getOption($name) {
    return $this->wrapped->getOption($name);
  }

  public function getUri() {
    return $this->wrapped->getUri();
  }

  public function toString($collect_bubbleable_metadata = FALSE) {
    return $this->wrapped->toString($collect_bubbleable_metadata);
  }

  public function toRenderArray() {
    return $this->wrapped->toRenderArray();
  }

  public function getInternalPath() {
    return $this->wrapped->getInternalPath();
  }

  public function access(AccountInterface $account = NULL) {
    return $this->wrapped->access($account);
  }

}
