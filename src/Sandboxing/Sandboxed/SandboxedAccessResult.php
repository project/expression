<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\expression\Sandboxing\SandboxerInterface;

final class SandboxedAccessResult extends SandboxedWrapperBase {

  /**
   * @var \Drupal\Core\Access\AccessResultInterface
   */
  protected $wrapped;

  public function __construct(AccessResultInterface $wrapped) {
    $this->wrapped = $wrapped;
  }

  public function isAllowed() {
    return $this->wrapped->isAllowed();
  }

  public function isForbidden() {
    return $this->wrapped->isForbidden();
  }

  public function isNeutral() {
    return $this->wrapped->isNeutral();
  }

  public function orIf(AccessResultInterface $other) {
    return $this->wrapped->orIf($other);
  }

  public function andIf(AccessResultInterface $other) {
    return $this->wrapped->andIf($other);
  }

}
