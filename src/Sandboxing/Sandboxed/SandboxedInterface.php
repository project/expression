<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

/**
 * Marker interface guaranteeing all class methods are safe
 */
interface SandboxedInterface {}
