<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

final class SandboxedUnknownObject extends SandboxedWrapperBase {

  public function __construct(object $wrapped) {
    $this->wrapped = $wrapped;
  }

}
