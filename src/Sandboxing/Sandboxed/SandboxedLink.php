<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Link;

final class SandboxedLink extends SandboxedWrapperBase {

  /**
   * @var \Drupal\Core\Link
   */
  protected $wrapped;

  public function __construct(Link $wrapped) {
    $this->wrapped = $wrapped;
  }

  public function getText() {
    return $this->wrapped->getText();
  }

  public function getUrl() {
    return $this->wrapped->getUrl();
  }

  public function toString() {
    return $this->wrapped->toString();
  }

  public function toRenderable() {
    return $this->wrapped->toRenderable();
  }

}
