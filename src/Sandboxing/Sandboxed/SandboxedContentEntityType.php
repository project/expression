<?php

namespace Drupal\expression\Sandboxing\Sandboxed;

use Drupal\Core\Entity\ContentEntityTypeInterface;

final class SandboxedContentEntityType extends SandboxedWrapperBase {

  use SandboxedContentEntityTypeTrait;

  public function __construct(ContentEntityTypeInterface $wrapped) {
    $this->wrapped = $wrapped;
  }

}
