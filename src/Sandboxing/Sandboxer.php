<?php

namespace Drupal\expression\Sandboxing;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Link;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\Url;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedAccessResult;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedConfigEntity;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedConfigEntityType;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedConfigurableLanguage;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedContentEntity;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedContentEntityType;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedEntity;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedFieldItem;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedFieldItemList;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedInterface;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedLanguage;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedLink;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedParameterBag;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedTypedData;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedUnknownObject;
use Drupal\expression\Sandboxing\Sandboxed\SandboxedUrl;
use Drupal\language\ConfigurableLanguageInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

final class Sandboxer implements SandboxerInterface {

  const MAP = [
    ParameterBag::class => SandboxedParameterBag::class,
    AccessResultInterface::class => SandboxedAccessResult::class,
    Url::class => SandboxedUrl::class,
    Link::class => SandboxedLink::class,
    ConfigurableLanguageInterface::class => SandboxedConfigurableLanguage::class,
    LanguageInterface::class => SandboxedLanguage::class,
    ConfigEntityInterface::class => SandboxedConfigEntity::class,
    ContentEntityInterface::class => SandboxedContentEntity::class,
    EntityInterface::class => SandboxedEntity::class,
    ConfigEntityTypeInterface::class => SandboxedConfigEntityType::class,
    ContentEntityTypeInterface::class => SandboxedContentEntityType::class,
    EntityTypeInterface::class => SandboxedContentEntityType::class,
    FieldItemListInterface::class => SandboxedFieldItemList::class,
    FieldItemInterface::class => SandboxedFieldItem::class,
    TypedDataInterface::class => SandboxedTypedData::class,
  ];

  public function wrap($item) {
    if ($item instanceof SandboxedInterface) {
      return $item;
    }
    elseif (is_object($item)) {
      foreach (self::MAP as $interface => $wrapperClass) {
        if ($item instanceof $interface) {
          return new $wrapperClass($item);
        }
      }
      return new SandboxedUnknownObject($item);
    }
    elseif (is_array($item)) {
      return array_map(function ($child) {
        return $this->wrap($child);
      }, $item);
    }
    // Item is scalar or resource, so nothing to do.
    return $item;
  }

}
